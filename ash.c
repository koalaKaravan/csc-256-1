/*
 * ash.c - Adorable Shell <3 <3 <3
 * --------------------------------
 * Sean Brennan <sbrennan>
 * sbrennan@u.rochester.edu
 * 9-12-12
 * --------------------------------
 * simple small shell. based on my 252 shell project.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <termios.h>
#include "ash.h"

/* groovy constants */
#define MAXARGS 128     /* max args on a command line */
#define MAXJOBS 16      /* max jobs at any point in time */
#define MAXJID  1 << 16 /* max job ID */
#define VERBOSE 0       /* zero this out for final version */

//Verify system calls
#define VERIFY(E) { \
  int stat = (E); \
  if (stat < 0) { \
  int err = errno; \
  fprintf(stderr, "%s[%d] bad return(%d/%d): %s\n", \
    __FILE__, __LINE__, stat, err, strerror(err)); \
  exit(-1); \
  }}

/* job states
 * valid state transitions:
 * FG -> ST : ctrl+z
 * ST -> FG : fg <pid or jid>
 * ST -> BG : bg <pid or jid>
 * BG -> fg : fg <pid or jid>
 */
#define UNDEF 0 /* undefined */
#define FG    1 /* running in foreground */
#define BG    2 /* running in background */
#define ST    3 /* stopped */

/* groovy globs */
extern char **environ;
const char *prompt = "ash$ ";
job_t jobs[MAXJOBS];
int nextjid = 1;

/*
 * main: here we here we go...
 */
int main(int argc, char **argv) {
  int i;
  char cmdline[MAXLINE];

  //dup2(1, 2);

  /* "zero out" jobs list */
  for(i = 0; i < MAXJOBS; i++)
    clearJob(&jobs[i]);

  /* bind signal handlers */
  bindSignal(SIGINT, sigint);
  bindSignal(SIGTSTP, sigtstp);
  bindSignal(SIGCHLD, sigchld);
  bindSignal(SIGQUIT, sigquit);
  signal(SIGTTOU, SIG_IGN); // ignore SIGTTOU

  /* shell end */
  atexit(killall);

  /* REPL */
  while(1) {
    /* Read command line */
    printf("[%s] %s", getcwd(0, 0), prompt);
    fflush(stdout);

    if ((fgets(cmdline, MAXLINE, stdin) == NULL) && ferror(stdin))
      app_error("fgets error");
    if (feof(stdin)) { /* End of file (ctrl-d) */
      fflush(stdout);
      exit(0);
    }

    eval(cmdline);
    fflush(stdout);
    fflush(stdout);
  }

  /* NO MAN'S LAND */
  exit(0);
}

/*
 * eval: parse and execute user command
 */
void eval(char *cmd) {
  char *argv[MAXARGS];
  int argc = parse(cmd, argv);
  int isBg = (argc < 0);
  argc *= isBg ? -1 : 1;
  pid_t pid, hpid;

  // if we do a built in, we're done.
  if(isBuiltinCmd(argv)) return;

  // break down the cmd into constituent piped procs
  int i, j;
  char ***procs = (char***) malloc(sizeof(char**) * MAXARGS);
  for(i = 0; i < MAXARGS; i++)
    procs[i] = (char**) malloc(sizeof(char*) * MAXARGS);
  int nProcs = parseProcs(argc, argv, procs);
  int nPipes = nProcs - 1;

  // make sure all commands are valid
  /*for(i = 0; i < nProcs; i++) {
    if(access(procs[i][0], F_OK | R_OK | X_OK) == -1) {
      fprintf(stderr, "%s: ", procs[i][0]);
      fprintf(stderr, "Command not found\n");
      return;
    }
  }*/

  // allocate space for pipes
  int doPipe = nPipes > 0;
  int **pipes;
  if(doPipe) {
    pipes = (int**) malloc(sizeof(int*) * (nProcs - 1));
    for(i = 0; i < nPipes; i++) {
      pipes[i] = (int*) malloc(sizeof(int) * 2);
      pipe(pipes[i]);
    }
  } else {
    pipes = NULL;
  }

  // create new child process(es)
  for(i = 0; i < nProcs; i++) {
    pid = fork();
    VERIFY(pid);

    // ADD JOB BEFORE ANYTHING ELSE
    if(pid != 0 && i == 0) {
      addJob(pid, (isBg ? BG : FG), cmd, pipes, nProcs);
      hpid = pid;
      VERIFY(setpgid(pid, pid));
    }

    if(pid == 0) {
      // set proper pgid
      // first proc: pgid == pid
      // other procs: pgid == hpid
      if(i == 0) {
        pid_t pgrpid = setpgrp();
        VERIFY(pgrpid);
        VERIFY(setpgid(0, pgrpid));

        // if fg then move controlling terminal to our pgrpid.
        if(!isBg) {
          tcsetpgrp(0, getpgrp());
          tcsetpgrp(1, getpgrp());
        }
      } else {
        VERIFY(setpgid(0, hpid));
      }

      // set read end of the pipe to stdin
      if(i > 0 && i <= nPipes) {
        close(pipes[i - 1][1]);
        dup2(pipes[i - 1][0], 0);
      }
      // make new pipe, set output end of the pipe to stdout
      if(i >= 0 && i < nPipes) {
        close(pipes[i][0]);
        dup2(pipes[i][1], 1);
      }

      // close unrelated pipes
      for(j = 0; j < nPipes; j++) {
        if(j - 1 != i && j != i) {
          close(pipes[j][0]);
          close(pipes[j][1]);
        }
      }

      //fprintf(stderr, "closing pipes: %d\n", getpid());

      // morph child process per command
      int err = execvp(procs[i][0], procs[i]);
      // control only reaches here if there was a problem creating the new proc,
      // print a message and quit
      fprintf(stderr, "Problem executing new process. ");
      fprintf(stderr, "Please make sure you typed a valid command. ");
      fprintf(stderr, "[ERRNO: %d]\n", err);
      fflush(stderr);
      exit(err);
    } else {
      // register this proc as subproc
      job_t *job = getJobByPid(hpid);
      pushPid(job, pid);
    } 
  }

  // wait for last fg child to finish
  if(!isBg)
    waitfg(pid);
  
  return;
}

/* 
 * fgbg: switch focus between fg & bg
 */
void fgbg(char **argv) {
  // determine if this is a bg or fg command.
  int doBg = 0;
  if(strcmp(argv[0], "bg") == 0)
    doBg = 1;
  
  // Make sure we have a valid command.
  if(argv[1] == '\0'){
    if(doBg)
      fprintf(stderr,"bg command requires PID or %%jobid argument\n");
    else
      fprintf(stderr,"fg command requires PID or %%jobid argument\n");
    return;
  }

  // Determine whether it is a jid or pid and operate.  
  int temp = 0;
  int isPid = 0;
  char* buf = argv[1];
  
  // Skip the % for jids.
  if('%' == argv[1][0])
    buf++;
  else
    isPid = 1;
  
  // Parse as int.
  if(sscanf(buf, "%d", &temp) != 1){
    if(doBg)
      fprintf(stderr,"bg: argument must be an integer.\n");
    else
      fprintf(stderr,"fg: argument must be an integer.\n");
    return;
  }
  
  // Get the job struct for this pid/jid.
  job_t* job = isPid ? getJobByPid(temp) : getJobByJid(temp);
  
  // Operate on this job.
  if(job) {
    // ST -> BG: continue and print message.
    if(job->state == ST && doBg){
      job->state = BG;
      VERIFY(kill((pid_t) (job->pid * -1), SIGCONT));
      printf("[%d] (%d) %s", job->jid, job->pid, job->cmdline);
      fflush(stdout);
    }
    
    // fg operations:
    pid_t pgid = job->pid;
    
    // BG -> FG: give it the terminal and wait.
    if(job->state == BG && !doBg){
      job->state = FG;
      tcsetpgrp(0, pgid);
      tcsetpgrp(1, pgid);
      waitfg(job->pid);
    }
    // ST -> FG: continue, give it the terminal and hang.
    else if(job->state == ST && !doBg){
      job->state = FG;
      tcsetpgrp(0, pgid);
      tcsetpgrp(1, pgid);
      VERIFY(kill((pid_t) (pgid * -1), SIGCONT));
      waitfg(job->pid);
    }
  } else {
    if(isPid)
      fprintf(stderr, "(%d): No such process\n", temp);
    else
      fprintf(stderr, "%%%d: No such job\n", temp);
    return;
  }  
  
  return;
}

/*
 * waitfg: spin our wheels until FG job is done
 */
void waitfg(pid_t pid) {
  job_t *job = getJobByPid(pid);
  while(job->state == FG) {
    pause();
  }

  return;
}

/*
 * changeDir: move to another directory
 */
void changeDir(char *dir) {
  if(chdir(dir) != 0)
    unix_error("no such directory");
  return;
}

/*
 * killall: make sure all running jobs are killed
 */
void killall(void) {
  int i;
  for (i = 0; i < MAXJOBS; i++) {
    if (jobs[i].pid != 0) {
      VERIFY(kill(jobs[i].pid * -1, SIGKILL));
      clearJob(&jobs[i]);
    }
  }
}

/*
 * parse: tokenize shell command
 */
int parse(const char *line, char **argv) {
    static char array[MAXLINE]; /* holds local copy of command line */
    char *buf = array;          /* ptr that traverses command line */
    char *delim;                /* points to first space delimiter */
    int argc;                   /* number of args */
    int bg;                     /* background job? */

    strcpy(buf, line);
    buf[strlen(buf)-1] = ' ';  /* replace trailing '\n' with space */
    while (*buf && (*buf == ' ')) /* ignore leading spaces */
      buf++;

    /* Build the argv list */
    argc = 0;
    if (*buf == '\'') {
      buf++;
      delim = strchr(buf, '\'');
    } else {
      delim = strchr(buf, ' ');
    }

    while (delim) {
      argv[argc++] = buf;
      *delim = '\0';
      buf = delim + 1;
      while (*buf && (*buf == ' ')) /* ignore spaces */
        buf++;

      if (*buf == '\'') {
        buf++;
        delim = strchr(buf, '\'');
      } else {
        delim = strchr(buf, ' ');
      }
    }

    argv[argc] = NULL;
    if (argc == 0)  /* ignore blank line */
      return 1;

    /* should the job run in the background? */
    if ((bg = (*argv[argc-1] == '&')) != 0)
      argv[--argc] = NULL;

    return bg ? argc * -1 : argc;
}

/*
 * parsePr0c: sift through the commands
 */
int parseProcs(int nTokens, char **tokens, char ***procs) {
  int pCount = 0, cmdc = 0, i;

  for(i = 0; i < nTokens; i++) {
    if(tokens[i][0] == '|') {
      pCount++;
      cmdc = 0;
    } else {
      procs[pCount][cmdc] = tokens[i];
      cmdc++;
    }
  }

  return pCount + 1;
}

/*
 * isBuiltinCmd: detect whether the user has typed a builtin cmd,
 * and if so, execute it
 */
int isBuiltinCmd(char **argv) {
  if(!argv[0]) return 6;

  if(strcmp(argv[0], "fg") == 0) {
    fgbg(argv); return 1;
  } else if(strcmp(argv[0], "bg") == 0) {
    fgbg(argv); return 2;
  } else if(strcmp(argv[0], "jobs") == 0) {
    printJobs(); return 3;
  } else if(strcmp(argv[0], "exit") == 0) {
    exit(0); return 4;
  } else if(strcmp(argv[0], "cd") == 0) {
    changeDir(argv[1]); return 5;
  }

  return 0;
}

/*
 * clearJob: zero out job
 */
void clearJob(job_t *job) {
  // clear out pipes first
  if(job->pipes) {
    int i;
    for(i = 0; i < job->subc - 1; i++)
      free(job->pipes[i]);
    free(job->pipes);
  }
  job->pid = 0;
  job->jid = 0;
  job->state = UNDEF;
  job->cmdline[0] = '\0';
  if(job->sub) free(job->sub);
  job->sub = 0;
  job->subc = 0;
  job->donec = 0;
}

/*
 * addJob: add new job to list
 */
int addJob(pid_t pid, int state, char *cmd, int** pipes, int subc) {
  int i;

  if (pid < 1)
    return 0;

  for (i = 0; i < MAXJOBS; i++) {
    if (jobs[i].pid == 0) {
      jobs[i].pid = pid;
      jobs[i].state = state;
      jobs[i].jid = nextjid++;
      if (nextjid > MAXJOBS)
        nextjid = 1;
      strcpy(jobs[i].cmdline, cmd);
      if(VERBOSE){
        printf("Added job [%d] %d %s\n", jobs[i].jid, jobs[i].pid, jobs[i].cmdline);
      }
      jobs[i].sub = (pid_t*) malloc(sizeof(pid_t) * subc);
      jobs[i].subc = subc;
      jobs[i].donec = 0;
      jobs[i].pipes = pipes;
      int j;
      for(j = 0; j < subc; j++)
        jobs[i].sub[j] = (pid_t) -1;

      // print notification on starting a backgrounded job.
      if(state == BG){
        printf("[%d] (%d) ", jobs[i].jid, jobs[i].pid);
        printf("%s", jobs[i].cmdline);
        fflush(stdout);
      }
      
      return 1;
    }
  }
  
  printf("There are already too many jobs running!\n");
  return 0;
}

/*
 * deleteJob: remove job from list
 */
int deleteJob(pid_t pid) {
  /*pid_t hpid = getpgid(pid);
  int i;
  
  if (pid < 1)
    return 0;

  for (i = 0; i < MAXJOBS; i++) {
    if (jobs[i].pid == hpid) {
      clearJob(&jobs[i]);
      nextjid = maxjid() + 1;
      return 1;
    }
  }
  
  return 0;*/
  if(pid < 1)
    return 0;
  job_t *job = getJobByPid(pid);
  if(job) {
    clearJob(job);
    nextjid = maxjid() + 1;
    return 1;
  }
  return 0;
}

/*
 * hasPid: find out if pid is member of job
 */
int hasPid(job_t *job, pid_t pid) {
  int i;
  
  if(pid < 1)
    return 0;
  if(job->pid == pid)
    return 1;
  for(i = 0; i <  MAXARGS; i++) {
    if(job->pid == 0) continue;
    if(job->sub[i] == pid)
      return 1;
  }

  return 0;
}

/*
 * getJobByPid: get job with passed pid
 */
job_t *getJobByPid(pid_t pid) {
  /*pid_t hpid = getpgid(pid);
  VERIFY(hpid);*/
  int i;

  if (pid < 1)
    return NULL;
  for (i = 0; i < MAXJOBS; i++)
    if (hasPid(&jobs[i], pid))
      return &jobs[i];
  
  return NULL;
}

/*
 * getJobByJid: get job with passed jid
 */
job_t *getJobByJid(int jid) {
  int i;

  if (jid < 1)
    return NULL;
  for (i = 0; i < MAXJOBS; i++)
    if (jobs[i].jid == jid)
      return &jobs[i];
  
  return NULL;
}

/*
 * pidToJid: get jid of proc with this pid
 */
int pidToJid(pid_t pid) {
  /*pid_t hpid = getpgid(pid);
  int i;

  if (pid < 1)
    return 0;
  for (i = 0; i < MAXJOBS; i++)
    if (jobs[i].pid == hpid) {
      return jobs[i].jid;
    }
  
  return 0;*/
  if(pid < 1)
    return 0;
  job_t *job = getJobByPid(pid);
  if(job)
    return job->jid;
  return 0;
}

/*
 * fgPid: get pid of foreground job
 */
pid_t fgPid() {
  int i;

  for (i = 0; i < MAXJOBS; i++)
    if (jobs[i].state == FG)
      return jobs[i].pid;
  
  return 0;
}

/*
 * maxJid: largest current JID
 */
int maxjid() {
  int i, max=0;
  
  for (i = 0; i < MAXJOBS; i++)
    if (jobs[i].jid > max)
      max = jobs[i].jid;
  
  return max;
}

/*
 * pushPid: register job subproc in list
 */
int pushPid(job_t *job, pid_t pid) {
  int i;

  for(i = 0; i < MAXARGS; i++) {
    if(job->sub[i] == (pid_t) -1) {
      job->sub[i] = pid;
      return 1;
    }
  }

  return 0;
}

/*
 * popPid: register job subproc as finished
 */
int popPid(job_t *job, pid_t pid) {
  int i;

  for(i = 0; i < MAXARGS; i++) {
    if(job->sub[i] == pid) {
      job->sub[i] = (pid_t) -1;
      job->donec++;
      return i;
    }
  }

  return 0;
}

/*
 * printJobs: print all jobs to terminal
 */
void printJobs() {
  int i;
    
  for (i = 0; i < MAXJOBS; i++) {
    if (jobs[i].pid != 0) {
      printf("[%d] (%d) ", jobs[i].jid, jobs[i].pid);
      
      switch (jobs[i].state) {
        case BG: 
          printf("Running ");
          break;
        case FG: 
          printf("Foreground ");
          break;
        case ST: 
          printf("Stopped ");
          break;
        default:
          printf("listjobs: Internal error: job[%d].state=%d ", 
            i, jobs[i].state);
      }
      
      printf("%s", jobs[i].cmdline);
    }
  }
}

/*
 * handler for SIGCHLD
 */
void sigchld(int sig) {
  pid_t cpid;
  job_t *cjob = NULL;
  int status;

  // IMPORTANT: block receipt of incoming signals
  // until we're done handling this one!!!
  /*sigset_t currentMask, previousMask;
  sigemptyset(&currentMask);
  sigaddset(&currentMask, SIGCHLD);
  sigprocmask(SIG_BLOCK, &currentMask, &previousMask);*/

  // continue until all zombie children are reaped
  // WNOHANG makes sure we don't wait for "living" children to die,
  // not sure if WUNTRACED is necessary
  while((cpid = waitpid((pid_t) -1, &status, WNOHANG | WUNTRACED)) > 0) {
    VERIFY(cpid);
    cjob = getJobByPid(cpid);
    //fprintf(stderr, "%d\n", cpid);
    if(!cjob) {
      fprintf(stderr, "couldn't find job for child process\n");
      return;
    }

    // child exited of its own accord:
    // give shell terminal and delete job.
    if(WIFEXITED(status)) {
      // pop pid from job list and delete job if finished
      int index = popPid(cjob, cpid);
      //fprintf(stderr, "pipe %d: [%d, %d]\n", index, cjob->pipes[index][0], cjob->pipes[index][1]);
      // IMPORTANT: close ends of the pipe!
      if(index > 0 && index <= cjob->subc - 1)
        close(cjob->pipes[index - 1][0]);
      if(index >= 0 && index < cjob->subc - 1)
        close(cjob->pipes[index][1]);
      if(cjob->donec >= cjob->subc) {
        if(cjob->state == FG) {
          tcsetpgrp(0, getpgrp());
          tcsetpgrp(1, getpgrp());
        }
        deleteJob(cjob->pid);
      }
    }

    // child exited due to uncaught signal:
    // give shell terminal if needed, delete its job and report its status
    if(WIFSIGNALED(status)) {
      if(cjob->state == FG){
        tcsetpgrp(0, getpgrp());
        tcsetpgrp(1, getpgrp());
      }
      
      // pop pid from job list and delete job if finished
      int index = popPid(cjob, cpid);
      //fprintf(stderr, "pipe %d: [%d, %d]\n", index, cjob->pipes[index][0], cjob->pipes[index][1]);
      // IMPORTANT: close ends of the pipe!
      if(index > 0 && index <= cjob->subc - 1)
        close(cjob->pipes[index - 1][0]);
      if(index >= 0 && index < cjob->subc - 1)
        close(cjob->pipes[index][1]);

      // pop pid from job list and delete job if finished
      // UNLESS!!! the sig was SIGPIPE. just proceed as usual
      // in that case
      if(WTERMSIG(status) != SIGPIPE) {
        // Print message about the termination.
        printf("Job [%d] (%d) ", cjob->jid, cpid);
        printf("terminated by signal %d\n", WTERMSIG(status));
        fflush(stdout);
        deleteJob(cjob->pid);
      }
    }

    // child is currently stopped:
    // Assume that child was stopped by a control+z, so grab back the control and report.
    if(WIFSTOPPED(status)) {
      if(cjob->state == FG) {
        tcsetpgrp(0, getpgrp());
        tcsetpgrp(1, getpgrp());
      }

      // Print message about the stop.
      printf("Job [%d] (%d) ", cjob->jid, cpid);
      printf("stopped by signal %d\n", WSTOPSIG(status));
      fflush(stdout);

      cjob->state = ST;
    }
  }
  
  // IMPORTANT: unmask signals!!!
  //sigprocmask(SIG_SETMASK, &previousMask, NULL);

  // trip the pause() in waitfg
  /*pid_t thisGuy = getpid();*/
  return;
}

/*
 * handler for SIGTSTP (ctrl+z)
 */
void sigtstp(int sig) {
  pid_t fpid = fgPid();

  if(fpid != 0) {
    VERIFY(kill(fpid * -1, sig));
    job_t* job = getJobByPid(fpid);
    job->state = ST;
  } else {
    printf("\n[%s] %s", getcwd(0, 0), prompt);
    fflush(stdout);
  }
  
  return;
}

/*
 * handler for SIGINT (ctrl+c)
 */
void sigint(int sig) {
  pid_t fpid = fgPid();

  // TODO: clear line if terminal is FG?
  if(fpid != 0) {
    VERIFY(kill(fpid * -1, sig));
  } else {
    printf("\n[%s] %s", getcwd(0, 0), prompt);
    fflush(stdout);
  }
  
  return;
}


/*
 * handler for SIGQUIT
 */
void sigquit(int sig) {
  printf("Sayonara my friends. ;_;\n");
  exit(1);
}

/*
 * bindSignal - do sigaction stuff
 */
handler_t *bindSignal(int signum, handler_t *handler) {
  struct sigaction action, old_action;

  action.sa_handler = handler;  
  sigemptyset(&action.sa_mask);
  action.sa_flags = SA_RESTART;

  if (sigaction(signum, &action, &old_action) < 0)
    unix_error("Signal error");

  return (old_action.sa_handler);
}

/*
 * unix_error: unix-style error routine
 */
void unix_error(char *msg) {
    fprintf(stdout, "%s: %s\n", msg, strerror(errno));
    exit(1);
}

/*
 * app_error: application-style error routine
 */
void app_error(char *msg) {
    fprintf(stdout, "%s\n", msg);
    exit(1);
}
