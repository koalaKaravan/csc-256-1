
PROC = ./proc_parse
ASH = ./ash
CC = gcc
FILES = $(PROC) $(ASH)

all: $(FILES)
proc: $(PROC)
ash: $(ASH)

clean:
	rm -f $(FILES) *.o *~


