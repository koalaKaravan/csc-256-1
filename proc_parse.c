
#include <math.h>
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include "proc_parse.h"
#define BUF_SIZE 256
#define DEBUG    0

int ends[2];

void main(int argc, char** argv) {
  if(argc != 1 && argc != 3) {
    printf("Usage: proc_parse [<read_rate> <printout_rate>]\n");
    exit(0);
  }

  if(argc == 1)
    simple();
  if(argc == 3) {
    int readRate, printoutRate;
    sscanf(argv[1], "%d", &readRate);
    sscanf(argv[2], "%d", &printoutRate);
    // create child process for regurgitating info
    // that we pipe its way
    if(pipe(ends) == -1) {
      printf("Issue occurred when attempting to open pipe :(\n");
      exit(-1);
    }

    if(fork() == 0) {
      close(ends[1]);
      continuousWrite(readRate, printoutRate, fdopen(ends[0], "r"));
    } else {
      close(ends[0]);
      continuousRead(readRate, fdopen(ends[1], "w"));
    }
  }
}

void simple() {
  FILE *cpuInfo, *version, *memInfo, *stat;
  char* line;

  // processor type
  cpuInfo = fopen("/proc/cpuinfo", "r");
  regex_t modelName; regcomp(&modelName, "^model name", 0);

  line = findFirstLine(cpuInfo, modelName);
  if(line != NULL) {
    printf("Processor Type: %s", line + 13);
    free(line);
  }
  
  fclose(cpuInfo);

  // kernel version
  version = fopen("/proc/sys/kernel/version", "r");
  regex_t only; regcomp(&only, "^", 0);

  line = findFirstLine(version, only);
  if(line != NULL) {
    printf("Kernel Information: %s", line);
    free(line);
  }
  
  fclose(version);

  // amount of memory
  memInfo = fopen("/proc/meminfo", "r");
  regex_t memTotal; regcomp(&memTotal, "^MemTotal:", 0);

  line = findFirstLine(memInfo, memTotal);
  if(line != NULL) {
    printf("Total Memory: %s", line + 10);
    free(line);
  }
  
  fclose(memInfo);

  // time since last boot
  stat = fopen("/proc/stat", "r");
  regex_t btime; regcomp(&btime, "^btime", 0);

  line = findFirstLine(stat, btime);
  if(line != NULL) {
    time_t statTime, currentTime = time(NULL);
    int bootTimeConv = 0;
    sscanf(line, "%*s %d", &bootTimeConv);
    statTime = (time_t) bootTimeConv;
    if(DEBUG) printf("Boot Time: %d\n", statTime);
    if(DEBUG) printf("Current Time: %d\n", currentTime);
    printf("Time Since Boot: %d seconds\n", currentTime - statTime);
    free(line);
  }
  
  fclose(stat);
}

void continuousRead(int readRate, FILE* pipe) {
  procAggregate *current, *previous = NULL;
  intervalStats *intv = NULL;

  // do initial read
  current = (procAggregate*) malloc(sizeof(procAggregate));
  fillAggregate(current);

  for(;;) {
    // gather information and pipe it down to child
    sleep(readRate);
    if(previous != NULL)
      free(previous);
    previous = current;
    current = (procAggregate*) malloc(sizeof(procAggregate));
    fillAggregate(current);
    intv = (intervalStats*) malloc(sizeof(intervalStats));
    fillInterval(intv, current, previous, readRate);
    if(DEBUG) {
      printf("INTV: %.5f %.5f %.5f %llu %.5f %.5f %.5f %.5f\n",
        intv->userPct, intv->sysPct, intv->idlePct, intv->freememTotal,
        intv->freememPct, intv->sectorRate, intv->switchRate, intv->createdRate);
    }
    fprintf(pipe, "%.5f %.5f %.5f %llu %.5f %.5f %.5f %.5f\n",
      intv->userPct, intv->sysPct, intv->idlePct, intv->freememTotal,
      intv->freememPct, intv->sectorRate, intv->switchRate, intv->createdRate);
    fflush(pipe);
    free(intv);
  }
}

void continuousWrite(int readRate, int printoutRate, FILE* pipe) {
  intervalStats *avg = (intervalStats*) malloc(sizeof(intervalStats));
  intervalStats *current = NULL;
  int totalTime = 0, i = readRate;
  char* line;

  for(;;) {
    // average printouts from parent and print
    sleep(printoutRate);
    int statCount = 0;
    totalTime += printoutRate;
    zeroInterval(avg);
    for(; i <= totalTime; i += readRate) {
      line = readLine(pipe);
      current = (intervalStats*) malloc(sizeof(intervalStats));
      sscanf(line, "%lf %lf %lf %llu %lf %lf %lf %lf",
        &(current->userPct), &(current->sysPct), &(current->idlePct),
        &(current->freememTotal), &(current->freememPct), &(current->sectorRate),
        &(current->switchRate), &(current->createdRate));
      if(DEBUG) {
        printf("Current: %f %f %f %llu %f %f %f %f\n", current->userPct,
          current->sysPct, current->idlePct, current->freememTotal, current->freememPct,
          current->sectorRate, current->switchRate, current->createdRate);
      }
      addInterval(avg, current);
      free(current);
      free(line);
      statCount++;
    }

    averageInterval(avg, statCount);
    if(DEBUG) {
      printf("Average: %f %f %f %llu %f %f %f %f <%d>\n\n", avg->userPct,
        avg->sysPct, avg->idlePct, avg->freememTotal, avg->freememPct,
        avg->sectorRate, avg->switchRate, avg->createdRate, statCount);
    }
    printf("SYSTEM AVERAGES (%d samples in %d seconds):\n", statCount, printoutRate);
    printf("%% User Time: %.5f%%\n", avg->userPct * 100);
    printf("%% System Time: %.5f%%\n", avg->sysPct * 100);
    printf("%% Idle Time: %.5f%%\n", avg->idlePct * 100);
    printf("Free Memory: %llu KB\n", avg->freememTotal);
    printf("%% Free Memory: %.5f%%\n", avg->freememPct * 100);
    printf("Sector Read/Write Rate: %.5f ops per sec\n", avg->sectorRate);
    printf("Context Switch Rate: %.5f switches per sec\n", avg->switchRate);
    printf("Process Creation Rate: %.5f procs per sec\n\n", avg->createdRate);
  }
}

void fillAggregate(procAggregate* agg) {
  FILE *stat, *dstat, *memInfo;
  char* line;

  regex_t procTime; regcomp(&procTime, "^cpu", 0);
  regex_t memFree; regcomp(&memFree, "^MemFree:", 0);
  regex_t memTotal; regcomp(&memTotal, "^MemTotal:", 0);
  regex_t sectors; regcomp(&sectors, "sda ", 0);
  regex_t switches; regcomp(&switches, "^ctxt", 0);
  regex_t created; regcomp(&created, "^processes", 0);

  // get info from /proc/stat
  stat = fopen("/proc/stat", "r");

  line = findFirstLine(stat, procTime);
  if(line != NULL) {
    // sum up all times so we dont get NaNs...
    unsigned long long a, b, c, d, e, f, g;
    sscanf(line, "cpu  %llu %llu %llu %llu %llu %llu %llu %llu %llu %llu",
      &(agg->userTime), &a, &(agg->sysTime), &(agg->idleTime), &b,
      &c, &d, &e, &f, &g);
    agg->totalTime = agg->userTime + agg->sysTime + agg->idleTime + a + b + c + d + e + f + g;
    free(line);
  }

  line = findFirstLine(stat, switches);
  if(line != NULL) {
    sscanf(line, "ctxt %llu", &(agg->switches));
    free(line);
  }

  line = findFirstLine(stat, created);
  if(line != NULL) {
    sscanf(line, "processes %llu", &(agg->processes));
    free(line);
  }

  fclose(stat);

  // get info from /proc/meminfo
  memInfo = fopen("/proc/meminfo", "r");

  line = findFirstLine(memInfo, memTotal);
  if(line != NULL) {
    sscanf(line, "MemTotal: %llu", &(agg->totalMem));
    free(line);
  }

  line = findFirstLine(memInfo, memFree);
  if(line != NULL) {
    sscanf(line, "MemFree: %llu", &(agg->freeMem));
    free(line);
  }

  fclose(memInfo);

  // get info from /proc/diskstats
  dstat = fopen("/proc/diskstats", "r");

  line = findFirstLine(dstat, sectors);
  if(line != NULL) {
    sscanf(line, "%*llu %*llu %*s %*llu %*llu %llu %*llu %*llu %*llu %llu", &(agg->sectorsRead), &(agg->sectorsWritten));
    agg->sectors = agg->sectorsRead + agg->sectorsWritten;
    free(line);
  }

  fclose(dstat);
}

void fillInterval(intervalStats* intv, procAggregate* current, procAggregate* previous,
    int readRate) {
  intv->proctimeSum = current->totalTime - previous->totalTime;
  intv->userPct = (double) (current->userTime - previous->userTime)
    / (double) intv->proctimeSum;
  intv->sysPct = (double) (current->sysTime - previous->sysTime)
    / (double) intv->proctimeSum;
  intv->idlePct = (double) (current->idleTime - previous->idleTime)
    / (double) intv->proctimeSum;
  intv->freememTotal = current->freeMem;
  intv->freememPct = (double) current->freeMem / (double) current->totalMem;
  intv->sectorRate = (double) (current->sectors - previous->sectors)
    / (double) readRate;
  intv->switchRate = (double) (current->switches - previous->switches)
    / (double) readRate;
  intv->createdRate = (double) (current->processes - previous->processes)
    / (double) readRate;
  if(DEBUG) {
    printf("CURRENT: %llu %llu %llu [%llu] %llukb [%llukb] %llu %llu [%llu] %llu %llu\n",
      current->userTime, current->sysTime, current->idleTime, current->totalTime,
      current->freeMem, current->totalMem, current->sectorsRead, current->sectorsWritten,
      current->sectors, current->switches, current->processes);
    printf("PREVIOUS: %llu %llu %llu [%llu] %llukb [%llukb] %llu %llu [%llu] %llu %llu\n",
      previous->userTime, previous->sysTime, previous->idleTime, previous->totalTime,
      previous->freeMem, previous->totalMem, previous->sectorsRead, previous->sectorsWritten,
      previous->sectors, previous->switches, previous->processes);
  }
}

void addInterval(intervalStats* total, intervalStats* intv) {
  total->proctimeSum += intv->proctimeSum;
  total->userPct += intv->userPct;
  total->sysPct += intv->sysPct;
  total->idlePct += intv->idlePct;
  total->freememTotal += intv->freememTotal;
  total->freememPct += intv->freememPct;
  total->sectorRate += intv->sectorRate;
  total->switchRate += intv->switchRate;
  total->createdRate += intv->createdRate;
}

void averageInterval(intervalStats* total, int amt) {
  total->proctimeSum /= (double) amt;
  total->userPct /= (double) amt;
  total->sysPct /= (double) amt;
  total->idlePct /= (double) amt;
  total->freememTotal /= (double) amt;
  total->freememPct /= (double) amt;
  total->sectorRate /= (double) amt;
  total->switchRate /= (double) amt;
  total->createdRate /= (double) amt;
}

void zeroInterval(intervalStats* intv) {
  intv->proctimeSum = 0;
  intv->userPct = 0;
  intv->sysPct = 0;
  intv->idlePct = 0;
  intv->freememTotal = 0;
  intv->freememPct = 0;
  intv->sectorRate = 0;
  intv->switchRate = 0;
  intv->createdRate = 0;
}

char* findFirstLine(FILE* file, regex_t match) {
  char* line;

  while((line = readLine(file)) && strlen(line) > 0) {
    if(!regexec(&match, line, 0, NULL, 0)) {
      return line;
    }
  }

  free(line);
  return NULL;
}

char* readLine(FILE* file) {
  char* buff = (char*) malloc(sizeof(char) * (BUF_SIZE + 1));
  char* cursor = buff;

  while(fgets(cursor, BUF_SIZE + 1, file) != NULL) {
    if(buff[strlen(buff) - 1] == '\n') {
      break;
    } else {
      buff = realloc(buff, strlen(buff) + BUF_SIZE + 1);
      cursor = buff + strlen(buff);
    }
  }
  
  return buff;
}
