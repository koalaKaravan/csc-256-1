
typedef struct {
  unsigned long long userTime;
  unsigned long long sysTime;
  unsigned long long idleTime;
  unsigned long long totalTime;
  unsigned long long freeMem;
  unsigned long long totalMem;
  unsigned long long sectorsRead;
  unsigned long long sectorsWritten;
  unsigned long long sectors;
  unsigned long long switches;
  unsigned long long processes;
} procAggregate;

typedef struct {
  // calculated data
  unsigned long long proctimeSum;
  double userPct;
  double sysPct;
  double idlePct;
  unsigned long long freememTotal;
  double freememPct;
  double sectorRate;
  double switchRate;
  double createdRate;
} intervalStats;

void simple();
void continuousRead(int readRate, FILE* pipe);
void continuousWrite(int readRate, int printoutRate, FILE* pipe);
void fillAggregate(procAggregate* agg);
void fillInterval(intervalStats* intv, procAggregate* current, procAggregate* previous, int readRate);
void addInterval(intervalStats* total, intervalStats* intv);
void averageInterval(intervalStats* total, int amt);
void zeroInterval(intervalStats* intv);

char* findFirstLine(FILE* file, regex_t pattern);
char* readLine(FILE* file);
