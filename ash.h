
#define MAXLINE 1024

typedef struct {
  pid_t pid;
  int jid;
  int state;
  char cmdline[MAXLINE];
  int subc;
  int donec;
  pid_t *sub;
  int** pipes;
} job_t;

typedef void handler_t(int);

/* Shell Essentials */
void eval(char *cmd);
void fgbg(char **cmd);
void waitfg(pid_t pid);
void changeDir(char *dir);
void killall(void);

/* Input Uitlities */
int parse(const char *line, char **argv);
int parseProcs(int nTokens, char **tokens, char ***procs);
int isBuiltinCmd(char **cmd);

/* Job Utilities */
void clearJob(job_t *job);
int addJob(pid_t pid, int state, char *cmd, int** pipes, int subc);
int deleteJob(pid_t pid);
int hasPid(job_t *job, pid_t pid);
job_t *getJobByPid(pid_t pid);
job_t *getJobByJid(int jid);
int pidToJid(pid_t pid);
int maxjid();
int pushPid(job_t *job, pid_t pid);
int popPid(job_t *job, pid_t pid);
pid_t fgPid();
void printJobs();

/* Signal Handlers */
void sigchld(int sig);
void sigtstp(int sig);
void sigint(int sig);
void sigquit(int sig);

/* Miscellaneous */
handler_t *bindSignal(int signum, handler_t *handler);
void unix_error(char *msg);
void app_error(char *msg);